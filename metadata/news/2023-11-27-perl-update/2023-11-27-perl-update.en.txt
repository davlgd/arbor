Title: Perl 5.38 update
Author: Heiko Becker <heirecka@exherbo.org>
Content-Type: text/plain
Posted: 2023-11-27
Revision: 1
News-Item-Format: 1.0
Display-If-Installed: dev-lang/perl[<5.38.0]

Perl was bumped to 5.38.0.

To switch to that version, run...

# eclectic perl set 5.38

As always after changing perl slots, you will need to rebuild all packages
that install Perl modules. To do this, you can run the following command
after switching to the new perl...

# cave resolve -1 $(for d in /usr/${CHOST}/lib/perl5/*/5.36-*; do cave print-owners "${d}" -f '%I\n'; done)

Replacing ${CHOST} with the triplet matching your system, and executing
those commands for any cross targets which Perl is installed to (or even
older perl slots).

In addition, if you installed any site-modules manually, you have to
reinstall those as well.
