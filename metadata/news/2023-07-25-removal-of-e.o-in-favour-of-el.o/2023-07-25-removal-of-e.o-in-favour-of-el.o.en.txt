Title: unavailable tarballs download location change
Author: Timo Gurr <tgurr@exherbo.org>
Content-Type: text/plain
Posted: 2023-07-25
Revision: 1
News-Item-Format: 1.0

Unfortunately due to the outage of our server hosting exherbo.org and the
lack of ownership of the domain we are in the process of transitioning to
exherbolinux.org until further notice and/or until we get control of
exherbo.org.

The sync URLs for the two unavailable{,-unofficial} repositories need to be
changed. They can (usually) be found at:

/etc/paludis/repositories/unavailable.conf
/etc/paludis/repositories/unavailable-unofficial.conf

and need to have git.exherbo.org replaced with unavailable.exherbolinux.org and
while doing so it's also advised to change tar+http to tar+https as well.

