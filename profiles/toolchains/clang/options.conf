# vim: set tw=80 et sw=4 sts=4 ts=4 fdm=marker fdr={{{,}}}

# {{{ Global defaults

# Always use llvm-libunwind
# The LLVM toolchain uses it, therefore it's not possible to use another
# libunwind implementation for other packages
*/* PROVIDERS: (llvm-libunwind)

# strace fails to build with llvm-libunwind due to missing libunwind-ptrace.h
# header, use elfutils-unwind provider
dev-util/strace PROVIDERS: elfutils-unwind

# }}}

# {{{ Global masks

# }}}

# {{{ Per-package defaults

# }}}

# {{{ Per-package masks

# }}}

# {{{ Per-package forces

# Marc-Antoine Perennou <keruspe@exherbo.org>
# Always use libc++ and compiler-rt
dev-lang/clang                  PROVIDERS: (compiler-rt) (-libgcc)
dev-lang/clang                  PROVIDERS: (libc++) (-libstdc++)
dev-lang/llvm                   PROVIDERS: (libc++) (-libstdc++)
dev-libs/compiler-rt            PROVIDERS: (compiler-rt) (-libgcc)
dev-libs/compiler-rt            PROVIDERS: (libc++) (-libstdc++)
sys-libs/libc++                 PROVIDERS: (compiler-rt) (-libgcc)
sys-libs/libc++abi              PROVIDERS: (compiler-rt) (-libgcc)
sys-libs/llvm-libunwind         PROVIDERS: (compiler-rt) (-libgcc)

dev-lang/php                    PROVIDERS: (libc++) (-libstdc++)
dev-lang/rust                   PROVIDERS: (libc++) (-libstdc++)
dev-lang/rust                   PROVIDERS: (llvm-libunwind) (-libgcc_s)
dev-util/elfutils               PROVIDERS: (libc++) (-libstdc++)

media-libs/openh264             PROVIDERS: (libc++) (-libstdc++)
media-libs/zimg                 PROVIDERS: (libc++) (-libstdc++)
wayland-apps/Waybar             PROVIDERS: (libc++) (-libstdc++)

x11-libs/qtbase                 PROVIDERS: (libc++)

# }}}

