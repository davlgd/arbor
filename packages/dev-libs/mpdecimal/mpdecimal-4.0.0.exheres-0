# Copyright 2024 Heiko Becker <heirecka@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

SUMMARY="A package for correctly-rounded arbitrary precision decimal floating point arithmetic"

HOMEPAGE="https://www.bytereef.org/mpdecimal/"
DOWNLOADS="https://www.bytereef.org/software/mpdecimal/releases/${PNV}.tar.gz"

LICENCES="BSD-2"
SLOT="0"
PLATFORMS="~amd64"
MYOPTIONS=""

DEPENDENCIES="
    build:
        app-arch/unzip
"

DEFAULT_SRC_CONFIGURE_PARAMS=(
    # x86_64-pc-linux-gnu-ld: unrecognized option '-Wl,-soname,libmpdec.so.4'
    LDXXFLAGS="${LDFLAGS}" LD="${CC}" LDXX="${CXX}"
    --disable-static
    --enable-cxx
)

src_fetch_extra() {
    if expecting_tests && [[ ! -e "${FETCHEDDIR}"/dectest.zip ]] ; then
        edo wget -O "${FETCHEDDIR}"/dectest.zip http://speleotrove.com/decimal/dectest.zip
    fi
}

src_unpack() {
    default

    if expecting_tests ; then
        edo mkdir "${WORK}"/tests/testdata
        edo pushd "${WORK}"/tests/testdata
        unpack dectest.zip
        edo popd
    fi
}

src_test() {
    emake check
}

