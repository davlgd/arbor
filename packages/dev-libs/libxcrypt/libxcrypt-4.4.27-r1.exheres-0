# Copyright 2018-2021 Timo Gurr <tgurr@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require github [ user=besser82 tag=v${PV} ] \
    autotools [ supported_autoconf=[ 2.7 ] supported_automake=[ 1.16 ] ] \
    alternatives

SUMMARY="Extended crypt library for DES, MD5, Blowfish and others"
DESCRIPTION="
libxcrypt is a modern library for one-way hashing of passwords. It supports DES, MD5, SHA-2-256,
SHA-2-512, and bcrypt-based password hashes, and provides the traditional Unix 'crypt' and
'crypt_r' interfaces, as well as a set of extended interfaces pioneered by Openwall Linux,
'crypt_rn', 'crypt_ra', 'crypt_gensalt', 'crypt_gensalt_rn', and 'crypt_gensalt_ra'.

libxcrypt is intended to be used by login(1), passwd(1), and other similar programs; that is, to
hash a small number of passwords during an interactive authentication dialogue with a human. It is
not suitable for use in bulk password-cracking applications, or in any other situation where speed
is more important than careful handling of sensitive data. However, it *is* intended to be fast and
lightweight enough for use in servers that must field thousands of login attempts per minute.

On Linux-based systems, by default libxcrypt will be binary backward compatible with the
libcrypt.so.1 shipped as part of the GNU C Library. This means that all existing binary executables
linked against glibc's libcrypt should work unmodified with this library's libcrypt.so.1. We have
taken pains to provide exactly the same \"symbol versions\" as were used by glibc on various CPU
architectures, and to account for the variety of ways in which the Openwall extensions were patched
into glibc's libcrypt by some Linux distributions. (For instance, compatibility symlinks for SuSE's
\"libowcrypt\" are provided.)

However, the converse is not true: programs linked against libxcrypt will not work with glibc's
libcrypt.  Also, programs that use certain legacy APIs supplied by glibc's libcrypt ('encrypt',
'encrypt_r', 'setkey', 'setkey_r', and 'fcrypt') cannot be compiled against libxcrypt.
"

LICENCES="
    BSD-2
    BSD-3
    FSFAP
    GPL-3
    LGPL-2.1 [[ note = [ library ] ]]
    public-domain
"
SLOT="0"
PLATFORMS="~amd64 ~armv7 ~armv8 ~x86"
MYOPTIONS=""

DEPENDENCIES="
    build:
        dev-lang/perl:*[>=5.14.0]
    build+run:
        !sys-libs/glibc[<2.33-r9] [[
            description = [ Part of glibc when built with the discouraged crypt functionality ]
            resolution = uninstall-blocked-after
        ]]
        !sys-libs/glibc[>=2.34&<2.34-r20] [[
            description = [ Part of glibc when built with the discouraged crypt functionality ]
            resolution = uninstall-blocked-after
        ]]
    run:
        !dev-libs/libxcrypt:0[<4.4.27-r1] [[
            description = [ Alternatives conflict ]
            resolution = upgrade-blocked-before
        ]]
"

DEFAULT_SRC_PREPARE_PATCHES=(
    "${FILES}"/ce562f4d33dc090fcd8f6ea1af3ba32cdc2b3c9c.patch
    "${FILES}"/95d6e03ae37f4ec948474d111105bbdd2938aba2.patch
)

DEFAULT_SRC_CONFIGURE_PARAMS=(
    --enable-failure-tokens
    --enable-shared
    --enable-hashes=all
    --enable-obsolete-api=glibc
    --enable-obsolete-api-enosys=no
    --enable-xcrypt-compat-files
    --disable-static
    --disable-valgrind
    --disable-werror
)

src_install() {
    local arch_dependent_alternatives=() man_pages=()
    local host=$(exhost --target)

    default

    arch_dependent_alternatives+=(
        /usr/${host}/include/crypt.h           crypt-${SLOT}.h
        /usr/${host}/include/xcrypt.h          xcrypt-${SLOT}.h
        /usr/${host}/lib/libcrypt.la           libcrypt-${SLOT}.la
        /usr/${host}/lib/libcrypt.so           libcrypt-${SLOT}.so
        /usr/${host}/lib/${PN}.so              ${PN}-${SLOT}.so
        /usr/${host}/lib/pkgconfig/libcrypt.pc libcrypt-${SLOT}.pc
        /usr/${host}/lib/pkgconfig/${PN}.pc    ${PN}-${SLOT}.pc
    )

    for page in crypt crypt_checksalt crypt_gensalt crypt_gensalt_ra \
        crypt_gensalt_rn crypt_preferred_method crypt_r crypt_ra crypt_rn ; do
        man_pages+=( /usr/share/man/man3/${page}.3 ${page}-${SLOT}.3 )
    done
    man_pages+=( /usr/share/man/man5/crypt.5 crypt-${SLOT}.5 )

    alternatives_for _${host}_${PN} ${SLOT} ${SLOT} "${arch_dependent_alternatives[@]}"
    alternatives_for _${PN} ${SLOT} ${SLOT} "${man_pages[@]}"
}

