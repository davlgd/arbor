# Copyright 2017 Marc-Antoine Perennou <keruspe@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

MY_PV="${PV:0:4}-${PV:4:2}-${PV:6:2}"

require github [ user=google tag=${MY_PV} ]
if ever at_least 20230901 ; then
    require cmake
fi
require alternatives

export_exlib_phases src_install

SUMMARY="RE2 is a fast, safe, thread-friendly alternative to backtracking regular expression engines"

LICENCES="BSD-3"
MYOPTIONS=""

DEPENDENCIES="
    run:
        !dev-libs/re2:0[<20200101-r1] [[
            description = [ Alternatives conflict ]
            resolution = upgrade-blocked-before
        ]]
"

if ever at_least 20230901 ; then
    DEPENDENCIES+="
        build:
            virtual/pkg-config
        build+run:
            dev-cpp/abseil
            dev-libs/icu:=
        test:
            dev-cpp/benchmark
            dev-cpp/gtest
    "
fi

if ever at_least 20230901 ; then
    CMAKE_SRC_CONFIGURE_PARAMS+=(
        -DRE2_USE_ICU:BOOL=TRUE
        -DBUILD_SHARED_LIBS:BOOL=TRUE
    )
    CMAKE_SRC_CONFIGURE_TESTS+=(
        '-DRE2_BUILD_TESTING:BOOL=TRUE -DRE2_BUILD_TESTING:BOOL=FALSE'
    )
else
    DEFAULT_SRC_INSTALL_PARAMS=( prefix=/usr/$(exhost --target) )
fi

re2_src_install() {
    local arch_dependent_alternatives=() host=$(exhost --target)

    if ever at_least 20230901 ; then
        cmake_src_install
    else
        default
    fi

    arch_dependent_alternatives+=(
        /usr/${host}/include/${PN}          ${PN}-${SLOT}
        /usr/${host}/lib/lib${PN}.so        ${PN}-${SLOT}.so
        /usr/${host}/lib/pkgconfig/${PN}.pc ${PN}-${SLOT}.pc
    )
    ever at_least 20230901 || \
        arch_dependent_alternatives+=( /usr/${host}/lib/lib${PN}.a ${PN}-${SLOT}.a )

    alternatives_for _${host}_${PN} ${SLOT} ${SLOT} "${arch_dependent_alternatives[@]}"
}

