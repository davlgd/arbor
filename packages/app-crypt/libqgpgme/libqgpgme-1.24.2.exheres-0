# Copyright 2022 Heiko Becker <heirecka@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

SUMMARY="A library for making GPG easier to use"
HOMEPAGE="https://www.gnupg.org/related_software/gpgme"
DOWNLOADS="mirror://gnupg/gpgme/gpgme-${PV}.tar.bz2"

LICENCES="GPL-2"
SLOT="0"
PLATFORMS="~amd64 ~armv8 ~x86"
MYOPTIONS="
    doc [[ description = [ Build API docs for the Qt bindings ] ]]
    ( providers: qt5 qt6 ) [[ number-selected = at-least-one ]]
"

DEPENDENCIES="
    build:
        doc? (
            app-doc/doxygen
            media-gfx/graphviz
        )
        virtual/pkg-config
    build+run:
        app-crypt/gpgme[>=${PV}]
        app-crypt/gnupg[>=2.0.4]
        dev-libs/libassuan:=[>=2.4.2]
        dev-libs/libgpg-error[>=1.47]
        providers:qt5? ( x11-libs/qtbase:5 )
        providers:qt6? ( x11-libs/qtbase:6 )
        !app-crypt/gpgme[qt5] [[
            description = [ libqgpgme was split out of app-crypt/gpgme ]
            resolution = uninstall-blocked-after
        ]]
"

WORK="${WORKBASE}"/gpgme-${PV}

_libqgpgme_config() {
    local params=(
        "$@"
        --disable-gpg-test
        --disable-gpgsm-test
        --enable-no-direct-extern-access
        --with-libassuan-prefix=/usr/$(exhost --target)
        --with-libgpg-error-prefix=/usr/$(exhost --target)
        $(option doc ac_cv_prog_DOXYGEN=doxygen ac_cv_prog_DOXYGEN=)
    )

    econf "${params[@]}"
}

src_configure() {
    ECONF_SOURCE="${WORK}"

    for qt in qt5 qt6  ; do
        if option providers:${qt} ; then
            echo "Configuring ${qt^} build:"
            edo mkdir "${WORKBASE}"/${qt}-build
            edo pushd "${WORKBASE}"/${qt}-build
            # qt bindings depend on cpp
            _libqgpgme_config "--enable-languages=cpp,${qt}"
            edo popd
        else
            echo "Configuring ${qt^} build disabled"
        fi
    done
}

src_compile() {
    for qt in qt5 qt6  ; do
        if option providers:${qt} ; then
            echo "Compiling ${qt^} build:"
            edo pushd "${WORKBASE}"/${qt}-build
            CC_FOR_BUILD=$(exhost --build)-cc emake
            edo popd
        else
            echo "Compiling ${qt^} disabled"
        fi
    done
}

src_test() {
    for qt in qt5 qt6  ; do
        if option providers:${qt} ; then
            echo "Testing ${qt^} build:"

            QT_WORK="${WORKBASE}"/${qt}-build
            edo pushd "${QT_WORK}"

            # Allow the tests to run its own instance of gpg-agent
            esandbox allow_net "unix:${QT_WORK}/tests/gpg/S.gpg-agent*"
            esandbox allow_net --connect "unix:${QT_WORK}/tests/gpg/S.gpg-agent*"
            esandbox allow_net "unix:${QT_WORK}/tests/gpgsm/S.gpg-agent*"
            esandbox allow_net --connect "unix:${QT_WORK}/tests/gpgsm/S.gpg-agent*"
            esandbox allow_net "unix:${QT_WORK}/tests/gpg*/S.dirmngr*"
            esandbox allow_net --connect "unix:${QT_WORK}/tests/gpg*/S.dirmngr*"

            esandbox allow_net "unix:${QT_WORK}/lang/qt/tests/S.gpg-agent*"
            esandbox allow_net --connect "unix:${QT_WORK}/lang/qt/tests/S.gpg-agent*"
            esandbox allow_net "unix:${TEMP}/t-tofuinfo-*/S.gpg-agent*"
            esandbox allow_net --connect "unix:${TEMP}/t-tofuinfo-*/S.gpg-agent*"
            esandbox allow_net "unix:${TEMP}/t-various-*/S.gpg-agent*"
            esandbox allow_net --connect "unix:${TEMP}/t-various-*/S.gpg-agent*"
            esandbox allow_net "unix:${QT_WORK}/lang/qt/tests/S.dirmngr*"
            esandbox allow_net --connect "unix:${QT_WORK}/lang/qt/tests/S.dirmngr*"

            emake check

            esandbox disallow_net --connect "unix:${QT_WORK}/lang/qt/tests/S.dirmngr*"
            esandbox disallow_net "unix:${QT_WORK}/lang/qt/tests/S.dirmngr*"
            esandbox disallow_net --connect "unix:${TEMP}/t-various-*/S.gpg-agent*"
            esandbox disallow_net "unix:${TEMP}/t-various-*/S.gpg-agent*"
            esandbox disallow_net --connect "unix:${TEMP}/t-tofuinfo-*/S.gpg-agent*"
            esandbox disallow_net "unix:${TEMP}/t-tofuinfo-*/S.gpg-agent*"
            esandbox disallow_net --connect "unix:${QT_WORK}/lang/qt/tests/S.gpg-agent*"
            esandbox disallow_net "unix:${QT_WORK}/lang/qt/tests/S.gpg-agent*"

            esandbox disallow_net --connect "unix:${QT_WORK}/tests/gpg*/S.dirmngr*"
            esandbox disallow_net "unix:${QT_WORK}/tests/gpg*/S.dirmngr*"
            esandbox disallow_net --connect "unix:${QT_WORK}/tests/gpgsm/S.gpg-agent*"
            esandbox disallow_net "unix:${QT_WORK}/tests/gpgsm/S.gpg-agent*"
            esandbox disallow_net --connect "unix:${QT_WORK}/tests/gpg/S.gpg-agent*"
            esandbox disallow_net "unix:${QT_WORK}/tests/gpg/S.gpg-agent*"

            edo popd
        else
            echo "Testing ${qt^} build disabled"
        fi
    done
}

src_install() {
    for qt in qt5 qt6  ; do
        if option providers:${qt} ; then
            echo "Installing ${qt^} build:"

            edo pushd "${WORKBASE}"/${qt}-build
            emake -j1 DESTDIR="${IMAGE}" install

            local host=$(exhost --target)
            # Remove the parts we already have from app-crypt/gpgme
            edo pushd "${IMAGE}"/usr/
            edo rm -r ${host}/include/gpgme++ \
                    ${host}/lib/cmake/Gpgmepp
            edo rm ${host}/bin/gpgme-* \
                ${host}/include/gpgme.h \
                ${host}/lib/{libgpgme.*,libgpgmepp.*} \
                ${host}/lib/pkgconfig/gpgme*.pc
            edo rmdir ${host}/{bin,lib/pkgconfig}

            edo rm share/aclocal/gpgme.m4 share/info/gpgme.info* \
                share/man/man1/gpgme-json.1
            edo rmdir share/aclocal share/man/man1 share/man
            edo popd

            if option doc ; then
                edo pushd lang/qt/doc/generated
                dodoc -r html
                edo popd
            fi
            edo popd
        else
            echo "Installing ${qt^} build disabled"
        fi
    done

    emagicdocs
}

