# Copyright 2019 Marc-Antoine Perennou <keruspe@exherbo.org>
# Copyright 2018 Bjorn Pagen <bjornpagen@gmail.com>
# Copyright 2018 Rasmus Thomsen <cogitri@exherbo.org>
# Copyright 2015 Johannes Nixdorf <mixi@exherbo.org>
# Copyright 2012 Elias Pipping <pipping@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require llvm-project [ slotted=true asserts=true stdlib=true ]
require alternatives

export_exlib_phases src_unpack src_prepare src_configure src_compile src_test src_test_expensive src_install

SUMMARY="The LLVM Compiler Infrastructure"

MYOPTIONS+="
    gold [[ description = [ Build the LLVM gold plugin ] ]]
    libedit
    polly [[ description = [ High-Level Loop and Data-Locality Optimizations ] ]]
    zstd
"

# FIXME: need a way to specify "when cross-compiling need llvm for build host" dependency
DEPENDENCIES+="
    build:
        dev-lang/perl:*
        dev-python/setuptools:*[python_abis:*(-)?]
        sys-devel/flex
        gold? ( sys-devel/binutils )
    build+run:
        dev-libs/libxml2:2.0
        sys-libs/zlib
        libedit? ( dev-libs/libedit )
        zstd? ( app-arch/zstd )
        !sys-devel/binutils[=2.30] [[
            description = [ gold regressed in 2.30 ]
            resolution = upgrade-blocked-before
        ]]
    test:
        dev-python/psutil[python_abis:*(-)?]
    run:
        !dev-lang/llvm:0[<5.0.1-r1] [[
            description = [ Old, unslotted llvm not supported ]
            resolution = upgrade-blocked-before
        ]]
    post:
        app-admin/eclectic-llvm
"
# FIXME: fails to find some missing headers?
#        polly? ( dev-libs/isl:=[>=0.20] )

CMAKE_SRC_CONFIGURE_PARAMS+=(
    # TODO(compnerd) hidden inline visibility causes test tools to fail to build as a required
    # method is hidden; move the definition out of line, and export the interface
    -DSUPPORTS_FVISIBILITY_INLINES_HIDDEN_FLAG:BOOL=OFF

    -DLLVM_DEFAULT_TARGET_TRIPLE:STRING=$(exhost --target)
    -DLLVM_INCLUDE_TESTS:BOOL=TRUE

    # We always want to build LLVM's dylib, which is a shared library
    # that basically contains all of the split libraries. It's required
    # for most stuff that wants to link against LLVM.
    -DLLVM_BUILD_LLVM_DYLIB:BOOL=TRUE

    # install LLVM to a slotted directory to prevent collisions with other llvm's
    -DCMAKE_INSTALL_PREFIX:STRING=${LLVM_PREFIX}
    -DCMAKE_INSTALL_DATAROOTDIR:STRING=${LLVM_PREFIX}/share
    -DCMAKE_INSTALL_MANDIR:STRING=${LLVM_PREFIX}/share/man

    # install utils (FileCheck, count, not) to `llvm-config --bindir`, so that
    # clang and others can use them
    -DLLVM_INSTALL_UTILS:BOOL=TRUE

    # needs perfmon2
    -DLLVM_ENABLE_LIBPFM:BOOL=FALSE

    -DLLVM_ENABLE_LIBXML2:BOOL=TRUE
    -DLLVM_ENABLE_WERROR:BOOL=FALSE
)

CMAKE_SRC_CONFIGURE_OPTIONS+=(
    'libedit LLVM_ENABLE_LIBEDIT'
    'polly LLVM_TOOL_POLLY_BUILD'
    'polly LLVM_POLLY_LINK_INTO_TOOLS'
    'zstd LLVM_ENABLE_ZSTD'
)

if ever at_least 18.1.5 ; then
    CMAKE_SRC_CONFIGURE_PARAMS+=(
        -DLLVM_ENABLE_HTTPLIB:BOOL=FALSE
        -DLLVM_INCLUDE_SPIRV_TOOLS_TESTS:BOOL=FALSE
    )
fi

if ever at_least 17.0 ; then
    CMAKE_SRC_CONFIGURE_PARAMS+=(
        -DLLVM_INSTALL_GTEST:BOOL=TRUE
    )
fi

if ever at_least 16.0 ; then
    CMAKE_SRC_CONFIGURE_PARAMS+=(
        -DLLVM_HAVE_TFLITE:BOOL=FALSE
    )
fi

CMAKE_SRC_CONFIGURE_TESTS=(
    # Build tests in src_compile instead of src_test
    '-DLLVM_BUILD_TESTS:BOOL=ON -DLLVM_BUILD_TESTS:BOOL=OFF'
    '--expensive -DLLVM_ENABLE_EXPENSIVE_CHECKS:BOOL=ON -DLLVM_ENABLE_EXPENSIVE_CHECKS:BOOL=OFF'
)

llvm_src_unpack() {
    llvm-project_src_unpack

    option polly && edo ln -s "${WORKBASE}"/llvm-project/polly "${CMAKE_SOURCE}"/tools/polly
}

llvm_src_prepare() {
    cmake_src_prepare

    # Fix the use of dot
    edo sed -e 's/@DOT@//g' -i docs/doxygen.cfg.in

    # These tests fail if gcc is not in path
    edo sed -e "s/bugpoint/\0 --gcc=${CC}/" -i test/BugPoint/*.ll

    # TODO(marvs): Need to investigate why these tests fail
    #
    # Exit Code: 1
    # Command Output (stdout):
    # --
    # Read input file      : '/var/tmp/paludis/build/dev-lang-llvm-8.0.0rc2/work/llvm-8.0.0rc2.src/test/BugPoint/func-attrs.ll'
    # *** All input ok
    # Running selected passes on program to test for crash: Success!
    # Initializing execution environment: Found lli: /var/tmp/paludis/build/dev-lang-llvm-8.0.0rc2/work/build/bin/lli
    # Sorry, I can't automatically select a safe interpreter!
    #
    # Exiting.
    edo rm test/BugPoint/func-attrs-keyval.ll
    edo rm test/BugPoint/func-attrs.ll

    # These tests started to fail with GCC 12
    #
    # ******************** TEST 'LLVM :: ExecutionEngine/MCJIT/eh.ll' FAILED ********************
    # Script:
    # --
    # : 'RUN: at line 2';   /var/tmp/paludis/build/dev-lang-llvm-15-scm/work/build/bin/lli -jit-kind=mcjit /var/tmp/paludis/build/dev-lang-llvm-15-scm/work/llvm-project/llvm/test/ExecutionEngine/MCJIT/eh.ll
    # --
    # Exit Code: 134
    #
    # Command Output (stderr):
    # --
    # terminate called after throwing an instance of 'int'
    # PLEASE submit a bug report to https://github.com/llvm/llvm-project/issues/ and include the crash backtrace.
    # Stack dump:
    # 0.	Program arguments: /var/tmp/paludis/build/dev-lang-llvm-15-scm/work/build/bin/lli -jit-kind=mcjit /var/tmp/paludis/build/dev-lang-llvm-15-scm/work/llvm-project/llvm/test/ExecutionEngine/MCJIT/eh.ll
    #  #0 0x00007f7c37ed7fd4 PrintStackTraceSignalHandler(void*) Signals.cpp:0:0
    #  #1 0x00007f7c37ed529b SignalHandler(int) Signals.cpp:0:0
    #  #2 0x00007f7c36a8a6e0 (/usr/x86_64-pc-linux-gnu/lib/libc.so.6+0x3c6e0)
    #  #3 0x00007f7c36ada1bc (/usr/x86_64-pc-linux-gnu/lib/libc.so.6+0x8c1bc)
    #  #4 0x00007f7c36a8a636 raise (/usr/x86_64-pc-linux-gnu/lib/libc.so.6+0x3c636)
    #  #5 0x00007f7c36a747fa abort (/usr/x86_64-pc-linux-gnu/lib/libc.so.6+0x267fa)
    #  #6 0x00007f7c36cdc909 (/usr/x86_64-pc-linux-gnu/lib/libstdc++.so.6+0xa9909)
    #  #7 0x00007f7c36ce7e8a (/usr/x86_64-pc-linux-gnu/lib/libstdc++.so.6+0xb4e8a)
    #  #8 0x00007f7c36ce7ef5 (/usr/x86_64-pc-linux-gnu/lib/libstdc++.so.6+0xb4ef5)
    #  #9 0x00007f7c36ce8147 (/usr/x86_64-pc-linux-gnu/lib/libstdc++.so.6+0xb5147)
    # #10 0x00007f7c36e8c02d
    # /var/tmp/paludis/build/dev-lang-llvm-15-scm/work/build/test/ExecutionEngine/MCJIT/Output/eh.ll.script: line 1: 1512034 Aborted                 (core dumped) /var/tmp/paludis/build/dev-lang-llvm-15-scm/work/build/bin/lli -jit-kind=mcjit /var/tmp/paludis/build/dev-lang-llvm-15-scm/work/llvm-project/llvm/test/ExecutionEngine/MCJIT/eh.ll
    #
    edo rm test/ExecutionEngine/MCJIT/eh-lg-pic.ll
    edo rm test/ExecutionEngine/MCJIT/eh.ll
    edo rm test/ExecutionEngine/MCJIT/multi-module-eh-a.ll
    edo rm test/ExecutionEngine/MCJIT/remote/eh.ll

    if [[ $(exhost --target) == *-musl* ]]; then
        edo rm test/CodeGen/Hexagon/csr-stubs-spill-threshold.ll
        edo rm test/CodeGen/Hexagon/long-calls.ll
        edo rm test/CodeGen/Hexagon/mlong-calls.ll
        edo rm test/CodeGen/Hexagon/pic-regusage.ll
        edo rm test/CodeGen/Hexagon/runtime-stkchk.ll
        edo rm test/CodeGen/Hexagon/swp-memrefs-epilog.ll
        edo rm test/CodeGen/Hexagon/vararg-formal.ll
    fi

    # /var/tmp/paludis/build/dev-lang-llvm-14.0.6/work/llvm-project-llvmorg-14.0.6/llvm/test/Other/ChangePrinters/DotCfg/print-changed-dot-cfg.ll:134:30:
    # error: CHECK-DOT-CFG-SIMPLE-NEXT: expected string not found in input
    # ; CHECK-DOT-CFG-SIMPLE-NEXT: <a>2. PassManager&lt;llvm::Function&gt; on g ignored</a><br/>
    #                              ^
    # [...]
    # /var/tmp/paludis/build/dev-lang-llvm-14.0.6/work/build/test/Other/ChangePrinters/DotCfg/Output/print-changed-dot-cfg.ll.tmp/passes.html:10:2: note: possible intended match here
    #  <a>2. PassManager&lt;Function&gt; on g ignored</a><br/>
    #  ^
    edo rm test/Other/ChangePrinters/DotCfg/print-changed-dot-cfg.ll

    # Hangs
    edo rm -r test/tools/opt-viewer/
}

llvm_src_configure() {
    local args=()

    if option gold; then
        args+=(
            -DLLVM_BINUTILS_INCDIR:STRING=/usr/$(exhost --is-native -q || echo "$(exhost --build)/$(exhost --build)/")$(exhost --target)/include/
        )
    fi

    if option polly; then
        args+=(
            # FIXME: fails to build with our system isl
            -DPOLLY_BUNDLED_ISL:BOOL=ON
            -DPOLLY_ENABLE_GPGPU_CODEGEN:BOOL=OFF
        )
    fi

    if [[ $(exhost --target) == *-musl* ]]; then
        # Only part of glibc, libexecinfo is extracted from there, but it
        # doesn't appear maintained and is buried on Exherbo. In any case
        # avoid the automagicness.
        args+=( -DCMAKE_DISABLE_FIND_PACKAGE_Backtrace:BOOL=TRUE )
    fi

    if option providers:libc++; then
        args+=( -DLLVM_ENABLE_LIBCXX:BOOL=TRUE )
    else
        args+=( -DLLVM_ENABLE_LIBCXX:BOOL=FALSE )
    fi

    cmake_src_configure "${args[@]}"
}

llvm_src_compile() {
    cmake_src_compile

    edo pushd "${CMAKE_SOURCE}"/utils/lit
    setup-py_src_compile
    edo popd
}

llvm_src_test() {
    # Some tests create symlinks to files in nonexistent path and expect open to fail with ENOENT
    # rather than 'permission denied' which they get without this exception
    esandbox allow '/_bad'
    esandbox allow_net "unix:${TEMP%/}/lit-tmp-*/*.sock"


    # TODO(Cogitri): As far as I can see LLVM doesn't offer a seperate
    # command to run non-expensive or expensive tests, so I resorted
    # to the following:
    expecting_tests --expensive || llvm-project_src_test

    esandbox disallow_net "unix:${TEMP%/}/lit-tmp-*/*.sock"
    esandbox disallow '/_bad'
}

llvm_src_test_expensive() {
    llvm-project_src_test
}

llvm_src_install() {
    cmake_src_install

    # Make sure e.g. clang will not look for tools in the build directory
    edo sed \
        -e 's:^set(LLVM_TOOLS_BINARY_DIR .*)$:set(LLVM_TOOLS_BINARY_DIR '${LLVM_PREFIX}'/bin):' \
        -i "${IMAGE}${LLVM_PREFIX}"/lib/cmake/llvm/LLVMConfig.cmake

    # Symlink dynlibs to /usr/lib
    edo pushd "${IMAGE}${LLVM_PREFIX}/lib"
    for lib in $(ls libLLVM-*.so); do
        dosym "${LLVM_PREFIX}/lib/${lib}" "/usr/$(exhost --target)/lib/${lib}"
    done
    edo popd

    nonfatal edo rmdir "${IMAGE}${LLVM_PREFIX}"/include/llvm/TextAPI/MachO
    nonfatal edo rmdir "${IMAGE}${LLVM_PREFIX}"/include/llvm/BinaryFormat/WasmRelocs

    # Manage alternatives for llvm binaries and manpages
    alternatives=()

    edo pushd "${IMAGE}${LLVM_PREFIX}/bin"
    for bin in $(ls); do
        alternatives+=("/usr/$(exhost --target)/bin/${bin}" "${LLVM_PREFIX}/bin/${bin}")
    done
    edo popd

    if option gold; then
        # allows using the non-llvm-prefixed ar, nm, ranlib on lto objects
        alternatives+=(
            /usr/$(exhost --build)/$(exhost --target)/lib/bfd-plugins/LLVMgold.so
            "${LLVM_PREFIX/$(exhost --target)/$(exhost --build)}"/lib/LLVMgold.so
        )
    fi

    ALTERNATIVES_llvm_DESCRIPTION="Alternatives for LLVM"
    alternatives_for "llvm" "${SLOT}" "${SLOT}" "${alternatives[@]}"
}

