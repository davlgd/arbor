# Copyright 2021 Heiko Becker <heirecka@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require cargo launchpad [ project=brz ]
require py-pep517 [ backend=setuptools entrypoints=[ git-remote-bzr brz bzr-{receive,upload}-pack ] ]
require utf8-locale
require bash-completion zsh-completion

export_exlib_phases src_install

SUMMARY="A friendly powerful distributed version control system"
DESCRIPTION="
Breezy is a version control system implemented in Python with multi-format
support and an emphasis on hackability.
Currently, Breezy has built-in support for the Git and Bazaar file formats
and network protocols."

BASE_URL="https://breezy-vcs.org/"
HOMEPAGE="${BASE_URL} ${HOMEPAGE}"

LICENCES="GPL-2"
SLOT="0"
MYOPTIONS=""

DEPENDENCIES="
    build:
        dev-python/Cython[>=0.29][python_abis:*(-)?]
        dev-python/packaging[python_abis:*(-)?]
        dev-python/setuptools-gettext[>=0.1.4][python_abis:*(-)?]
        dev-python/setuptools-rust[python_abis:*(-)?]
    build+run:
        dev-python/configobj[python_abis:*(-)?]
        dev-python/dulwich[>=0.21.6][python_abis:*(-)?]
        dev-python/fastbencode[python_abis:*(-)?]
        dev-python/merge3[python_abis:*(-)?]
        dev-python/patiencediff[python_abis:*(-)?]
        dev-python/PyYAML[python_abis:*(-)?]
        dev-python/tzlocal[python_abis:*(-)?]
        dev-python/urllib3[>=1.24.1][python_abis:*(-)?]
    test:
        dev-python/testtools[python_abis:*(-)?]
        dev-python/testscenarios[python_abis:*(-)?]
        dev-util/subunit[python_abis:*(-)?]
    suggestion:
        (
            dev-python/paramiko[python_abis:*(-)?]
            dev-python/pycryptodome[python_abis:*(-)?]
        ) [[
            *description = [ Access branches over sftp ]
            *group-name = sftp
        ]]
        dev-scm/python-fastimport [[
            description = [ Support for the git fastimport format ]
        ]]
        dev-python/launchpadlib[>=1.6.3] [[
            description = [ Needed for interaction with the Launchpad API ]
        ]]
"
# NOTE: To PGP sign commits and very PGP signatures we would need
# gpgme[python], but we currently hard-disable that.

UPSTREAM_DOCUMENTATION="${BASE_URL}doc/en/"
UPSTREAM_RELEASE_NOTES="${BASE_URL}doc/en/release-notes/brz-$(ever range -2).html#brz-$(ever replace_all -)"

# There are a few tests (from a big number) failing, but I'm not that
# motivated to investigate. Please, feel free to do so.
RESTRICT="test"

unpack_one_multibuild() {
    default

    edo pushd "${PYTHON_WORK}"
    ecargo_fetch
    edo popd
}

compile_one_multibuild() {
    #PYTHON=${PYTHON} emake extensions

    py-pep517_compile_one_multibuild

    edo ${PYTHON} tools/generate_docs.py man

    if option bash-completion ; then
        edo ${PYTHON} tools/generate_docs.py bash_completion
    fi
}

breezy_src_install() {
    py-pep517_src_install
}

install_one_multibuild() {
    py-pep517_install_one_multibuild

    doman brz.1
    doman breezy/git/git-remote-bzr.1

    dobashcompletion brz.bash_completion brz

    # Install translations for the last python abi and into the correct
    # location.
    if [[ -d ${IMAGE}/usr/share/locale ]]; then
        edo rm -r "${IMAGE}"/usr/share/locale/*
    else
        edo mkdir "${IMAGE}"/usr/share/locale
    fi
    edo mv "${IMAGE}"/usr/$(exhost --target)/share/locale/* "${IMAGE}"/usr/share/locale
    edo rmdir "${IMAGE}"/usr/$(exhost --target)/{share/locale,share}
}

test_one_multibuild() {
    require_utf8_locale

    edo ${PYTHON} -m installer -d pep517_tests_${PN} -p /usr/$(exhost --target) dist/*.whl
    edo pushd pep517_tests_${PN}

    # Borrowed from .github/workflows
    PYTHONPATH=$(ls -d $(python_get_sitedir)) \
        edo ${PYTHON} \
        -Wignore::ImportWarning -Wignore::PendingDeprecationWarning -Wignore::DeprecationWarning -Wignore::ResourceWarning -Wignore::UserWarning \
        usr/$(exhost --target)/bin/brz --no-plugins selftest -v
    edo popd
}

