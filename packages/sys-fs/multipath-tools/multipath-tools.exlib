# Copyright 2009-2017 Wulf C. Krueger <philantrop@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require github [ user=opensvc ]
require udev-rules systemd-service
require flag-o-matic

export_exlib_phases pkg_setup src_prepare pkg_postinst

SUMMARY="Userland tools for the device Mapper multipathing driver"
DESCRIPTION="
If you don't know what this is, you don't want it. Unless you want kpartx which
is part of this package.
"
HOMEPAGE="http://christophe.varoqui.free.fr"

LICENCES="LGPL-2"
SLOT="0"
MYOPTIONS=""

DEPENDENCIES="
    build+run:
        dev-libs/json-c:=
        dev-libs/libaio[>=0.3.107]
        dev-libs/userspace-rcu
        sys-apps/systemd[>=245]
        sys-fs/lvm2[>=2.02.45]
        sys-fs/sysfsutils[>=2.1.0]
    test:
        dev-util/cmocka
"

DEFAULT_SRC_COMPILE_PARAMS=(
    CC="${CC}"
)

multipath-tools_pkg_setup() {
    # The test suite fails with LTO
    # https://github.com/opensvc/multipath-tools/issues/18
    filter-flags -flto
}

multipath-tools_src_prepare() {
    default

    edo sed \
        -e "s:\$(prefix):/usr/$(exhost --target):g" \
        -e "s:\$(usr_prefix)share/man:/usr/share/man:g" \
        -e "s:\$(LIB):/lib:g" \
        -e "s:sbin:bin:g" \
        -e "s:\$(systemd_prefix)lib/systemd/system:${SYSTEMDSYSTEMUNITDIR}:g" \
        -e "s:\$(systemd_prefix)lib/tmpfiles.d:${SYSTEMDTMPFILESDIR}:g" \
        -e "s:\$(systemd_prefix)lib/modules-load.d:/usr/$(exhost --target)/lib/modules-load.d:g" \
        -e "s:\$(systemd_prefix)lib/udev:${UDEVDIR}:" \
        -i Makefile.inc

    # prevent installing empty directory
    edo sed \
        -e '/-d $(DESTDIR)$(modulesloaddir)/d' \
        -i multipath/Makefile
}

multipath-tools_pkg_postinst() {
    local cruft=( /etc/udev/rules.d/{65-multipath,66-kpartx}.rules )
    for file in ${cruft[@]}; do
        if test -f "${file}" ; then
            nonfatal edo rm "${file}" || ewarn "removing ${file} failed"
        fi
    done
}

