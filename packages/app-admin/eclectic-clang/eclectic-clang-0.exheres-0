# Copyright 2018 Bjorn Pagen <bjornpagen@gmail.com>
# Copyright 2015 Kylie McClain <somasis@exherbo.org>
# Copyright 2013 Marc-Antoine Perennou <keruspe@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require alternatives

SUMMARY="Manages clang's symlinks"
HOMEPAGE="https://www.exherbo.org"
DOWNLOADS=""

LICENCES="GPL-2"
PLATFORMS="~amd64 ~armv7 ~armv8 ~x86"

SLOT="0"

CROSS_COMPILE_TARGETS="
    aarch64-unknown-linux-musleabi
    aarch64-unknown-linux-gnueabi
    armv7-unknown-linux-gnueabi
    armv7-unknown-linux-gnueabihf
    i686-pc-linux-gnu
    i686-pc-linux-musl
    powerpc64-unknown-linux-gnu
    riscv32-unknown-linux-gnu
    riscv64-unknown-linux-gnu
    x86_64-pc-linux-gnu
    x86_64-pc-linux-musl
"

MYOPTIONS="
    ( targets: ${CROSS_COMPILE_TARGETS} ) [[ number-selected = at-least-one ]]
"

DEPENDENCIES="
    run:
        app-admin/eclectic[>=2.0.20]
        dev-lang/clang:*
"

WORK="${WORKBASE}"

eclectic-clang_alternatives() {
    local target=${1} name=${2} realname=${3}

    local alternatives=(
        /usr/$(exhost --target)/bin/${target}-${name}   ${target}-${realname}
        /usr/share/man/man1/${target}-${name}.1         ${target}-${realname}.1
        /usr/$(exhost --target)/bin/${name}             ${realname}
        "${BANNEDDIR}"/${name}                          ${realname}
    )
    alternatives_for ${name} clang 500 "${alternatives[@]}"
}

src_install() {
    for target in ${CROSS_COMPILE_TARGETS};do
        if option targets:${target};then
            eclectic-clang_alternatives ${target} cc  clang
            eclectic-clang_alternatives ${target} c++ clang++
            eclectic-clang_alternatives ${target} cpp clang-cpp
        fi
    done
}

