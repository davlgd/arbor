# Copyright 2009, 2012, 2021, 2023, 2024 Ali Polatel <alip@chesswob.org>
# Distributed under the terms of the GNU General Public License v2

require alternatives

MY_PN=${PN%-bin}

SUMMARY="The ☮ther SⒶndbøx (binary build)"
DESCRIPTION="seccomp and landlock based application sandbox with support for namespaces"
HOMEPAGE="https://crates.io/crates/syd"
DOWNLOADS="
    platform:amd64? ( https://distfiles.exherbolinux.org/${MY_PN}/syd-${PV}-x86_64-pc-linux-gnu.tar.xz )
    platform:armv7? ( https://distfiles.exherbolinux.org/${MY_PN}/syd-${PV}-armv7-unknown-linux-gnueabihf.tar.xz )
    platform:armv8? ( https://distfiles.exherbolinux.org/${MY_PN}/syd-${PV}-aarch64-unknown-linux-gnueabi.tar.xz )
"


LICENCES="GPL-3"
SLOT="3"
MYOPTIONS="
    ( platform: amd64 armv7 armv8 )
"

DEPENDENCIES="
    build+run:
        !sys-apps/sydbox:0[<1.2.1-r2|>=3] [[
            description = [ Alternatives conflict ]
            resolution = upgrade-blocked-before
        ]]
        !sys-apps/sydbox-bin:0 [[
            description = [ Slot move ]
            resolution = uninstall-blocked-after
        ]]
"

ever at_least 3.8.8 || RESTRICT="strip"
WORK="${WORKBASE}/syd-${PV}-$(exhost --target)"
WORK="${WORK/musl/gnu}" # FIXME: tombriden: ENEEDLOVE! does this break metadata?

export_exlib_phases src_install src_test src_test_expensive pkg_postinst

sydbox-bin-3_src_install() {
    local provider=${MY_PN}-${SLOT}-bin
    for bin in bin/syd*; do
        name=${bin##*/}
        newbin ${bin} ${name}-${SLOT}.bin
        alternatives_for ${MY_PN} ${provider} 75 \
            /usr/$(exhost --target)/bin/${name} ${name}-${SLOT}.bin
    done
    alternatives_for ${MY_PN} ${provider} 75 \
        /usr/$(exhost --target)/bin/${MY_PN} syd-${SLOT}.bin

    insinto /usr/$(exhost --target)/libexec/${PN}
    doins esyd.sh

    emagicdocs
}

sydbox-bin-3_src_test() {
    if ! esandbox check 2>/dev/null; then
        pushd bin
        edo env CARGO_BIN_EXE_syd=$PWD/syd CARGO_BIN_EXE_syd-test-do=$PWD/syd-test-do ./syd-test
        popd
    else
        elog "Not running tests because SydB☮x doesn't work under SydB☮x"
        elog "Tests are installed by default, run with syd-test"
    fi
}

sydbox-bin-3_src_test_expensive() {
    if ! esandbox check 2>/dev/null; then
        pushd bin
        edo env SYD_TEST_EXPENSIVE=1 CARGO_BIN_EXE_syd=$PWD/syd CARGO_BIN_EXE_syd-test-do=$PWD/syd-test-do ./syd-test
        popd
    else
        elog "Not running expensive tests because SydB☮x doesn't work under SydB☮x"
        elog "Expensive tests are installed by default, run with \"syd-test exp\""
    fi
}

sydbox-bin-3_pkg_postinst() {
    alternatives_pkg_postinst

    elog 'To use SydB☮x in your shell session, a helper script is provided.'
    elog 'In your ~/.bashrc:'
    elog '--8<--'
    elog 'eval "$(syd --sh)"'
    elog '-->8--'
    elog 'Then run "esyd help" in a shell running under SydB☮x.'
}

