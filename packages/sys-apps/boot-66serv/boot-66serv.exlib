# Copyright 2020 Volodymyr Medvid <vmedvid@riseup.net>
# Distributed under the terms of the GNU General Public License v2

require gitlab [ prefix=https://framagit.org user=Obarun tag=v${PV} ]

export_exlib_phases src_compile pkg_postinst

SUMMARY="Portable set of service to boot a machine in conjunction with 66 API"
HOMEPAGE="https://framagit.org/Obarun/${PN}"

LICENCES="ISC"
SLOT="0"
MYOPTIONS=""

DEPENDENCIES="
    build:
        virtual/pkg-config
    build+run:
        dev-lang/execline[>=2.5.3.0]
        dev-libs/oblibs[>=0.0.5.0]
        dev-libs/skalibs[>=2.9.0.0]
        sys-apps/66-tools[>=0.0.3.0]
        sys-apps/66[>=0.2.4.0]
        sys-apps/iproute2
        sys-apps/kmod
        sys-apps/s6-linux-utils[>=2.5.1.1]
        sys-apps/s6-portable-utils[>=2.2.2.1]
        sys-apps/s6-rc[>=0.5.1.1]
        sys-apps/s6[>=2.9.0.1]
    suggestion:
        net-firewall/iptables [[ description = [ Restore iptables rules ] ]]
        sys-fs/btrfs-progs [[ description = [ Probe btrfs volumes at boot ] ]]
        sys-fs/lvm2 [[ description = [ Probe LVM devices at boot ] ]]
        sys-fs/zfs [[ description = [ Mount ZFS devices at boot ] ]]
"

DEFAULT_SRC_CONFIGURE_PARAMS=(
    --HOSTNAME=localhost
)

boot-66serv_src_compile()
{
    :
}

boot-66serv_pkg_postinst() {
    if [[ ! -d "${ROOT}/var/lib/66/system/boot" ]]; then
        einfo "boot-66serv supervision tree is not found."
        einfo "Execute the below commands to create the boot tree and enable all services:"
        einfo "66-tree -n boot"
        einfo "66-enable -t boot boot"
    fi
}

