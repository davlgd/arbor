# Copyright 2007 Bryan Østergaard <kloeri@exherbo.org>
# Copyright 2009-2011 Wulf C. Krueger <philantrop@exherbo.org>
# Copyright 2011,2013 Alex Elsayed <eternaleye@gmail.com>
# Copyright 2020 Ali Polatel <alip@exherbo.org>
# Distributed under the terms of the GNU General Public License v2
# Based in part upon 'syslog-ng-2.0.9.ebuild' from Gentoo, which is:
#     Copyright 1999-2008 Gentoo Foundation

require github [ release=${PNV} suffix=tar.gz ]
require openrc-service [ openrc_confd_files=[ "${FILES}"/openrc/confd ] ]
require systemd-service [ systemd_files=[ contrib/systemd/syslog-ng@.service ] ]

export_exlib_phases src_install pkg_postinst

SUMMARY="Popular system logger"
HOMEPAGE="https://www.syslog-ng.com"

LICENCES="GPL-2"
SLOT="0"
MYOPTIONS="
    amqp    [[ description = [ Output logs over AMQP ] ]]
    caps
    geoip   [[ description = [ Non-DNS IP-to-country resolving ] ]]
    mongodb [[ description = [ Write logs to a mongodb instance ] ]]
    pacct   [[ description = [ Read process accounting logs ] ]]
    sql     [[ description = [ Write logs to an SQL server ] ]]
    smtp    [[ description = [ Send mail as part of the logging pipeline ] ]]
    systemd
    tcpd
    ( providers: libressl openssl ) [[ number-selected = exactly-one ]]
"
RESTRICT="test"

DEPENDENCIES="
    build:
        sys-devel/bison[>=3.7.6] [[ note = [ maybe not needed due to pre-compiled .c ] ]]
        virtual/pkg-config
    build+run:
        !sys-apps/eventlog [[
            description = [ Bundled now, conflicting file: libevtlog.so ]
            resolution = uninstall-blocked-after
        ]]
        dev-libs/glib:2[>=2.32]
        dev-libs/ivykis[>=0.42.4]
        dev-libs/json-c:=[>=0.13]
        dev-libs/pcre2[>=10.0]
        net-misc/curl [[ note = [ http() destination support ] ]]
        amqp? ( net-libs/librabbitmq[>=0.8.0] )
        caps? ( sys-libs/libcap )
        geoip? ( net-libs/libmaxminddb )
        mongodb? ( net-libs/libmongoc[>=1.0.0] )
        smtp? ( net-libs/libesmtp )
        sql? ( dev-libs/libdbi[>=0.9.0] )
        systemd? ( sys-apps/systemd[>=245] )
        tcpd? ( sys-apps/tcp-wrappers[>=7.6] )
        providers:libressl? ( dev-libs/libressl:= )
        providers:openssl? ( dev-libs/openssl:=[>=0.9.8] )
    recommendation:
        app-admin/logrotate [[
            description = [ Rotate and remove old logs ]
        ]]
"

DEFAULT_SRC_INSTALL_EXTRA_DOCS=( contrib/syslog-ng.conf.doc )

DEFAULT_SRC_CONFIGURE_PARAMS=(
    --sysconfdir=/etc/syslog-ng

    --enable-cpp
    --enable-dynamic-linking
    --enable-ipv6
    --enable-json
    --enable-manpages
    --enable-manpages-install
    --disable-afsnmp
    --disable-cloud-auth
    --disable-darwin-osl
    --disable-env-wrapper
    --disable-ebpf
    --disable-example-modules
    --disable-force-classic-linking
    --disable-grpc
    --disable-java
    --disable-kafka
    --disable-mqtt
    --disable-python
    --disable-python-modules
    --disable-python-packages
    --disable-redis
    --disable-riemann
    --disable-spoof-source
    --disable-sun-streams
    --disable-valgrind
    --with-ivykis=system
    --with-jsonc
    --with-module-dir=/usr/$(exhost --target)/lib/syslog-ng
    --with-pidfile-dir=/run
    --without-libnet
    --without-sanitizer
)

DEFAULT_SRC_CONFIGURE_OPTION_ENABLES=(
    amqp
    "caps linux-caps"
    "geoip geoip2"
    mongodb
    pacct
    smtp
    sql
    systemd
    "tcpd tcp-wrapper"
)

DEFAULT_SRC_CONFIGURE_OPTION_WITHS=(
    "amqp librabbitmq-client"
    "mongodb libmongo-client system"
    "systemd systemdsystemunitdir ${SYSTEMDSYSTEMUNITDIR}"
)

syslog-ng_src_install() {
    default

    dodoc "${FILES}"/syslog-ng.conf.sample

    insinto /etc/logrotate.d
    newins "${FILES}"/syslog-ng.logrotate syslog-ng

    keepdir /etc/syslog-ng/patterndb.d
    keepdir /var/lib/syslog-ng
    keepdir /var/log

    if option systemd; then
        insinto /etc/default
        doins "${WORK}/contrib/systemd/syslog-ng@default"
    fi

    install_openrc_files
}

syslog-ng_pkg_postinst() {
    default

    if [[ -d "${ROOT}"/etc/env.d/alternatives/systemd-logger ]]; then
        nonfatal edo rm -r "${ROOT}"/etc/env.d/alternatives/systemd-logger || \
            ewarn "Removing /etc/env.d/alternatives/systemd-logger failed"
    fi
    if [[ -f "${ROOT}"/usr/share/eclectic/modules/auto/systemd-logger.eclectic ]]; then
        nonfatal edo rm "${ROOT}"/usr/share/eclectic/modules/auto/systemd-logger.eclectic || \
            ewarn "Removing /etc/env.d/alternatives/systemd-logger failed"
    fi
}

