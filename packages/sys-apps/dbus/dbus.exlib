# Copyright 2010 Saleem Abdulrasool <compnerd@compnerd.org>
# Copyright 2010-2015 Wulf C. Krueger <philantrop@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require meson
require openrc-service systemd-service s6-rc-service 66-service

export_exlib_phases src_prepare src_configure src_test src_install

SUMMARY="D-Bus is a message bus system for interprocess communication (IPC)"
DESCRIPTION="
D-Bus is a message bus system, a simple way for applications to talk to one another.
In addition to interprocess communication, D-Bus helps coordinate process lifecycle;
it makes it simple and reliable to code a 'single instance' application or daemon,
and to launch applications and daemons on demand when their services are needed.
D-Bus supplies both a system daemon (for events such as 'new hardware device added'
or 'printer queue changed') and a per-user-login-session daemon (for general IPC
needs among user applications).
"
HOMEPAGE="https://${PN}.freedesktop.org"
DOWNLOADS="${HOMEPAGE}/releases/${PN}/${PNV}.tar.xz"

LICENCES="|| ( GPL-2 AFL-2.1 )"
SLOT="0"
MYOPTIONS="
    apparmor
    debug
    doc
    X
    ( providers: consolekit elogind systemd ) [[
        *description = [ Session tracking provider ]
        number-selected = at-most-one
    ]]
"

DEPENDENCIES="
    build:
        app-text/docbook-xml-dtd:4.4
        dev-libs/libxslt
        virtual/pkg-config[>=0.20]
        doc? ( app-doc/doxygen )
        X? ( x11-proto/xorgproto )
    build+run:
        dev-libs/expat[>=2.1.0]
        sys-apps/skeleton-filesystem-layout[>=0.5.4]
        group/messagebus
        group/plugdev
        user/messagebus
        apparmor? ( security/apparmor[>=2.10] )
        providers:elogind? ( sys-auth/elogind[>=226] )
        providers:systemd? ( sys-apps/systemd[>=226] )
        X? (
            x11-libs/libICE
            x11-libs/libSM
            x11-libs/libX11
        )
    run:
        providers:consolekit? ( sys-auth/ConsoleKit2 )
"

DEFAULT_SRC_PREPARE_PATCHES=(
    "${FILES}"/dbus-enable-elogind.patch
)

dbus_src_prepare() {
    meson_src_prepare

    # fix cross
    edo sed \
        -e "s:/usr/bin/systemctl:/usr/$(exhost --target)/bin/systemctl:g" \
        -i meson.build
    edo sed \
        -e "s:get_option('prefix') / 'lib':get_option('libdir'):g" \
        -i bus/{sysusers,tmpfiles}.d/meson.build

    # The usual meson-docdir rant
    edo sed -e "/docs_dir = /s/'dbus'/'${PNVR}'/" -i meson.build
    edo sed \
        -e "s|\(install_dir: get_option('datadir') / 'doc' / \)'dbus'|\1'${PNVR}'|" \
        -i bus/meson.build
}

dbus_src_configure() {
    local meson_args=(
        --localstatedir=/var
        -Dchecks=true
        -Ddbus_daemondir=/usr/$(exhost --target)/bin
        -Ddbus_user=messagebus
        -Dducktype_docs=disabled
        -Depoll=enabled
        -Dinstalled_tests=false
        -Dintrusive_tests=false
        -Dinotify=enabled
        -Dkqueue=disabled
        -Dlaunchd=disabled
        -Dlibaudit=disabled
        -Dmessage_bus=true
        -Dmodular_tests=disabled
        -Dqt_help=disabled
        -Drelocation=disabled
        -Druntime_dir=/run
        -Dselinux=disabled
        -Dsession_socket_dir=/tmp
        -Dstats=true
        -Dsystem_pid_file=/run/dbus.pid
        -Dsystem_socket=/run/dbus/system_bus_socket
        -Dsystemd_system_unitdir=${SYSTEMDSYSTEMUNITDIR}
        -Dsystemd_user_unitdir=${SYSTEMDUSERUNITDIR}
        -Dtools=true
        -Dtraditional_activation=true
        -Dvalgrind=disabled
        -Dxml_docs=enabled
        $(meson_feature apparmor)
        $(meson_feature doc doxygen_docs)
        $(meson_feature providers:elogind elogind)
        $(meson_feature providers:systemd systemd)
        $(meson_feature X x11_autolaunch)
        $(meson_switch debug asserts)
        $(meson_switch debug verbose_mode)
    )

    if option providers:elogind || option providers:systemd; then
        meson_args+=( -Duser_session=true )
    else
        meson_args+=( -Duser_session=false )
    fi

    exmeson "${meson_args[@]}"
}

dbus_src_test() {
    esandbox allow_net "unix:${TEMP}/dbus-test-*/*"

    DBUS_VERBOSE=1 meson_src_test

    esandbox disallow_net "unix:${TEMP}/dbus-test-*/*"
}

dbus_src_install() {
    local host=$(exhost --target)

    meson_src_install

    if ! option doc ; then
        [[ -d "${IMAGE}"/usr/share/doc/${PNVR}/api ]] && edo rmdir "${IMAGE}"/usr/share/doc/${PNVR}/api
    fi

    # systemd 226 now supports the concept of user buses replacing session buses, if used with dbus-1.10
    # (and enabled via dbus -Duser_session=true)
    if ! option providers:elogind || ! option providers:systemd ; then
        # dbus X session script (Gentoo bug #77504)
        # turns out to only work for GDM. has been merged into other desktop
        # (kdm and such scripts)
        exeinto /etc/X11/xinit/xinitrc.d/
        doexe "${FILES}"/30-dbus
    fi

    keepdir \
        /usr/${host}/libexec/dbus-1 \
        /usr/share/dbus-1/services \
        /usr/share/dbus-1/session.d \
        /usr/share/dbus-1/system.d \
        /usr/share/dbus-1/system-services \
        /var/lib/dbus \
        /usr/${host}/lib/dbus-1.0/test

    edo rmdir "${IMAGE}"/run/{dbus,}

    # hardcodes the path to the setuid helper that is used to launch system services
    edo sed \
        -e "s#/$(exhost --target)/#/host/#g" \
        -i "${IMAGE}"/usr/share/dbus-1/system.conf

    # keepdir legacy locations until our packages are migrated
    keepdir /etc/dbus-1/{session,system}.d/

    install_openrc_files
    install_s6-rc_files
    install_66_files
}

